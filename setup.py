import os

from setuptools import find_packages, setup

readme = open('README.rst').read()

install_requires = [
    "rasterio>=1.2.10",
    "geopandas>=0.7.0",
    "requests>=2.26.0",
    "dagster==0.14.0"
]

packages = find_packages()

g = {}
with open(os.path.join('bdcp_utils', 'version.py'), 'rt') as fp:
    exec(fp.read(), g)
    version = g['__version__']

setup(
    name='bdcp-utils',
    version=version,
    description=__doc__,
    long_description=readme,
    keywords=[],
    license='MIT',
    author='Vitor Gomes',
    author_email='vconrado@gmail.com',
    url='',
    packages=packages,
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    entry_points={},
    install_requires=install_requires,
    classifiers=[],
)