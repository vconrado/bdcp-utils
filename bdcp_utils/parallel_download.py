import click
import requests
import shutil
import time
import json
from multiprocessing import Pool, cpu_count
from functools import partial
import os

def download_file(data: dict) -> None:
    start = time.time()
    print("Starting download ", data["file_path"])
    try:
        with requests.get(data["url"], stream=True) as r:
            with open(data["file_path"], "wb") as f:
                shutil.copyfileobj(r.raw, f, )
    except Exception as e:
        print(e)
    end = time.time()
    size = float(os.path.getsize(data['file_path']))
    print(f"Download time: {data['file_path']} {end - start} {(size/float(end-start))} B/s")


@click.command()
@click.option('-f', '--file')
@click.option('-c', '--cpu', default=None, type=int)
def download(file, cpu):

    with open(file) as json_file:
        items = json.load(json_file)

    if cpu is None:
        cpu = cpu_count()
        print(f"Using {cpu} cpus")
    
    pool = Pool(cpu)
    download_func = partial(download_file)
    pool.map(download_func, items["data"])
    pool.close()
    pool.join()
    
    

if __name__ == '__main__':
    start = time.time()
    download()
    end = time.time()
    print("Wall time: ", end-start)