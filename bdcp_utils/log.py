import logging
from dagster import get_dagster_logger

def get_logger() -> logging.Logger:
    return get_dagster_logger()