# -*- coding: utf-8 -*-
import os
import subprocess
import geopandas as gpd
from typing import List
import rasterio
import rasterio.mask
from multiprocessing import ProcessError
import requests
import shutil
import time
import tempfile
import json
import pathlib


class Utils:
    @staticmethod
    def gcs_download(base_url: str, dest_path: str, overwrite: bool = False) -> dict:
        """Download an scene from Google Cloud Storage using `gsutil` command line tool
        """
        # Executes gsutil
        command = ['gsutil', '-m', 'cp', '-r'] + ["-n"] * (not overwrite) + [base_url, dest_path]
        command = " ".join(command)
        print(command)
        proc = subprocess.run(command, shell = True, capture_output=True)

        if proc.returncode != 0:
            print(proc.stdout)
            raise ProcessError(proc.stderr)

        download_path = os.path.join(
            dest_path, base_url.split("/")[-1])

        return download_path

    @staticmethod
    def download_file(url: str, file_path: str) -> None:
        start = time.time()
        resume_byte_pos = 0
        try:
            resume_byte_pos = os.path.getsize(file_path)
            mtime = os.path.getmtime(file_path)
        except: pass

        headers = {}
        if resume_byte_pos:
            headers.update(Range='bytes=%d-' % resume_byte_pos)
            headers.update({"If-Unmodified-Since": time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime(mtime))})
            print("Requesting", headers)

        with requests.get(url, stream=True, headers=headers,verify=False, allow_redirects=True) as r:
            if r.status_code == 206:
                print('Resuming download')
                mode = 'ab'
            elif r.status_code == 200:
                print('(Re)starting download')
                mode = 'wb'
            else:
                raise Exception(f"Unexpected HTTP status while getting {url}: ", r)
            with open(file_path, mode) as f:
                shutil.copyfileobj(r.raw, f)
        end = time.time()
        print("Download time: ", end - start)

    @staticmethod
    def download_files_parallel(data, cpu) -> subprocess.CompletedProcess:

        with tempfile.NamedTemporaryFile() as tmp:
            content = {
                "data": data
            }
            with open(tmp.name, "w") as file:
                json.dump(content, file)

            file_path = pathlib.Path(__file__).parent.resolve()
            command = f"python {file_path}/parallel_download.py -f {tmp.name} -c {cpu}"

            return subprocess.run(command, shell = True, capture_output=True)
    
    @staticmethod
    def crop_image_by_shapefile(source_image_path: str, dest_image_path: str ,shapefile: gpd.GeoDataFrame):
        """
        Usando como referencia o método crop_images_by_shapefile do shapefile.py do pacote uranus
        """
        features = shapefile["geometry"].tolist()
       
        with rasterio.open(source_image_path) as src:
            out_image, out_transform = rasterio.mask.mask(src, features, crop=True)
            out_meta = src.meta.copy()
            out_meta.update({
                "driver": "GTiff",
                "height": out_image.shape[1],
                "width": out_image.shape[2],
                "transform": out_transform
            })
        # Salvando o resultado
        with rasterio.open(dest_image_path, "w", **out_meta) as dest:
            dest.write(out_image)

    @staticmethod
    def gdal_calc_run(bands: dict, calc:str, outputfile:str, conda_env=None, overwrite=False, no_data_value=None):
        """
            Ex: calc = (A-B)/(A+B)
        """
        command=""
        if conda_env:
            command = f"source activate {conda_env};"
        command+=f"gdal_calc.py --outfile={outputfile}"
        for key in bands:
            command+=f" -{key} {bands[key]} --{key}_band=1"
        command+=f" --calc=\"{calc}\""
        if overwrite:
            command+=" --overwrite"
        if no_data_value:
            command+=f" --NoDataValue={no_data_value}"
        return subprocess.run(command, shell = True, capture_output=True)

    @staticmethod
    def mtl_to_json(mtl_path):
        def parse(file):
            content={}
            for line in file:
                line = line.strip()
                if line.startswith("GROUP"):
                    group_name = line.split("=")[1].strip()
                    content[group_name] = parse(file)
                elif line.startswith("END"):
                    return content
                else:
                    key, value = line.split("=")
                    key = key.strip()
                    value = value.strip().strip('"')
                    content[key] = value

        with open(mtl_path, "r") as f: 
            content = parse(f)
        return content
